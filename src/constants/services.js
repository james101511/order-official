import React from "react"
import { FaCode, FaSketch, FaAndroid } from "react-icons/fa"
export default [
  {
    id: 1,
    icon: <FaCode className="service-icon" />,
    title: "QRCODE",
    text: `智能QRCODE點餐 ， 解決人力不足的捆擾`,
  },
  {
    id: 2,
    icon: <FaSketch className="service-icon" />,
    title: "報表",
    text: `每日訂單自動保存 ， 後台報表查詢`,
  },
  {
    id: 3,
    icon: <FaAndroid className="service-icon" />,
    title: "價位",
    text: `經濟實惠的價位，小資餐廳也能負擔`,
  },
]
